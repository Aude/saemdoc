========
Contenu
========

Bienvenue sur la documentation du projet SAEM! Ces documents sont organisés au sein de différents guides qui s'adressent à différents publics
.. image:: images/2050.jpg

.. toctree::
  :maxdepth: 2

  guide-utilisateur/index
  guide-installation/index

.. note::

Cette documentation est maintenue par l'équipe projet SAEM. Les modules SAEM et la documentation sont gratuites et open source et les contributions sont les bienvenues.
Pour contribuer à cette documentation, rendez-vous sur :doc:`/contributing/documentation`.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

